package com.spalmer.model.dto;

import com.spalmer.model.enums.NotificationName;

import java.util.Map;

public class NotificationDTO {

	private NotificationName name;
	private String email;
	private Map<String, String> content;

	public NotificationDTO(NotificationName name, String email, Map<String, String> content) {
		this.name = name;
		this.email = email;
		this.content = content;
	}

	public NotificationDTO() {
	}

	public static class NotificationDTOBuilder {

		private NotificationName name;
		private String email;
		private Map<String, String> content;

		public NotificationDTOBuilder name(NotificationName name) {
			this.name = name;
			return this;
		}

		public NotificationDTOBuilder email(String email) {
			this.email = email;
			return this;
		}

		public NotificationDTOBuilder content(Map<String, String> content) {
			this.content = content;
			return this;
		}

		public NotificationDTO createNotificationDTO() {
			return new NotificationDTO(name, email, content);
		}
	}

	public NotificationName getName() {
		return name;
	}

	public void setName(NotificationName name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Map<String, String> getContent() {
		return content;
	}

	public void setContent(Map<String, String> content) {
		this.content = content;
	}
}
