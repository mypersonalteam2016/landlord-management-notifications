package com.spalmer.controller;

import com.spalmer.model.dto.NotificationDTO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
public class NotificationsController {

	@RequestMapping(value = "/signup", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Map<String,String> notifications(@RequestBody NotificationDTO dto)  {
		return Collections.emptyMap();
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Map<String,String> getNotifications() {
		return Collections.emptyMap();
	}
}
