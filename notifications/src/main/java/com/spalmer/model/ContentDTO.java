package com.spalmer.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ContentDTO {
	@JsonProperty("template_id")
	private String templateId;

	public ContentDTO(String templateId) {
		this.templateId = templateId;
	}

	public String getTemplateId() {
		return this.templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}
}
