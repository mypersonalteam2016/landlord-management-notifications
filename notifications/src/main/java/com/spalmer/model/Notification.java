package com.spalmer.model;

//import com.spalmer.model.enums.NotificationName;
import org.springframework.data.annotation.Id;

public class Notification {

	@Id
	public String id;

//	private NotificationName name;

	private String templateName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

//	public NotificationName getName() {
//		return name;
//	}
//
//	public void setName(NotificationName name) {
//		this.name = name;
//	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
}
