//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.spalmer.model;

public class RecipientDTO {
	private String address;

	public RecipientDTO(String address) {
		this.address = address;
	}

	public RecipientDTO() {
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
