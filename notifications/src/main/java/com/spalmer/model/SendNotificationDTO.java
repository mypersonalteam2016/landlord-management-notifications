//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.spalmer.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class SendNotificationDTO {
	@JsonProperty("substitution_data")
	private SubstitutionDTO substitutions;
	private List<RecipientDTO> recipients;
	private ContentDTO content;

	public SendNotificationDTO(SubstitutionDTO substitutions, List<RecipientDTO> recipients, ContentDTO content) {
		this.substitutions = substitutions;
		this.recipients = recipients;
		this.content = content;
	}

	public SendNotificationDTO() {
	}

	public SubstitutionDTO getSubstitutions() {
		return this.substitutions;
	}

	public void setSubstitutions(SubstitutionDTO substitutions) {
		this.substitutions = substitutions;
	}

	public List<RecipientDTO> getRecipients() {
		return this.recipients;
	}

	public void setRecipients(List<RecipientDTO> recipients) {
		this.recipients = recipients;
	}

	public ContentDTO getContent() {
		return this.content;
	}

	public void setContent(ContentDTO content) {
		this.content = content;
	}

	public static class NotifcationDTOBuilder {
		private SubstitutionDTO substitutions;
		private List<RecipientDTO> recipients;
		private ContentDTO content;

		public NotifcationDTOBuilder() {
		}

		public SendNotificationDTO.NotifcationDTOBuilder substitutions(SubstitutionDTO substitutions) {
			this.substitutions = substitutions;
			return this;
		}

		public SendNotificationDTO.NotifcationDTOBuilder recipients(List<RecipientDTO> recipients) {
			this.recipients = recipients;
			return this;
		}

		public SendNotificationDTO.NotifcationDTOBuilder content(ContentDTO content) {
			this.content = content;
			return this;
		}

		public SendNotificationDTO build() {
			return new SendNotificationDTO(this.substitutions, this.recipients, this.content);
		}
	}
}
