//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.spalmer.model;

public class SubstitutionDTO {
	private String url;

	public SubstitutionDTO(String url) {
		this.url = url;
	}

	public SubstitutionDTO() {
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
