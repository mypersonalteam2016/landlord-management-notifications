package com.spalmer.repository;

import com.spalmer.model.Notification;
//import com.spalmer.model.enums.NotificationName;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface NotificationRepository extends MongoRepository<Notification, String> {

//	Optional<Notification> findByName(NotificationName name);
}
